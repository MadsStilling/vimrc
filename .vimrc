filetype on

call pathogen#infect()
call pathogen#helptags()

execute pathogen#infect()
syntax on
filetype plugin indent on

" This is used from a guide
" http://www.sontek.net/blog/2011/05/07/turning_vim_into_a_modern_python_ide.html#tab-completion-and-documentation

" Leader <leader> is changed to comma.
let mapleader=","

" Line numbers
set number

" Set pastemode
set pastetoggle=<F12>

" code folding
set foldmethod=indent
set foldlevel=99

" Window splits
" Vertical Split : Ctrl+w + v
" Horizontal Split: Ctrl+w + s
" Close current windows: Ctrl+w + q

" Quick pairs
nmap <leader>' i''<ESC>i
nmap <leader>" i""<ESC>i
nmap <leader>( i()<ESC>i
nmap <leader>[ i[]<ESC>i
nmap <leader>{ i{}<ESC>i

imap <leader>' ''<ESC>i
imap <leader>" ""<ESC>i
imap <leader>( ()<ESC>i
imap <leader>[ []<ESC>i
imap <leader>{ {}<ESC>i

" window navigation
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" NB! This controls
" if the arrow keys work
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>
ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>
vno <down> <Nop>
vno <left> <Nop>
vno <right> <Nop>
vno <up> <Nop>

" Centering line on navigation
nmap G Gzz
nmap n nzz
nmap N nzz
nmap } }zz
nmap { {zz

" TODO or FIXME's
" Now you can hit <leader>td to open your task list 
" and hit ‘q’ to close it. You can also hit enter 
" on the task to jump to the buffer and line that it is placed on.
map <leader>td <Plug>TaskList

" history
map <leader>g :GundoToggle<CR>

" Syntax highlighting and validation
syntax on                    " syntax highlighing
filetype on                  " try to detect filetypes
filetype plugin indent on    " enable loading indent file for filetype
let g:pyflakes_use_quickfix = 0

" Pep8
let g:pep8_map='<leader>8'

" Tab completion and documentation
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview

" File browser
map <leader>n :NERDTreeToggle<CR>

" Refactoring and Go to definition
map <leader>j :RopeGotoDefinition<CR>
map <leader>r :RopeRename<CR>

" Searching
nmap <leader>a <Esc>:Ack!

